F-Droid Update Detector
=======================
This library contains an `org.fdroid.appupdater.UpdateRequest` class that will help you determine if
your app has an update available from the supplied F-Droid repository catalog.

To use it, create an instance of `UpdateRequest.Builder` and configure it:

- Supply a `Context` to the constructor

- Call `fromCatalog(Uri)`, pointing to the catalog to examine for
possible updates

- Call `asJson()`, `asXml()`, or `parseUsing(CatalogParser)` to indicate
how we should parse the content identified by the `Uri` supplied to
`fromCatalog(Uri)`

- If the `Uri` has an `http` or `https` scheme, you can call
`downloadUsing(StreamDownloadStrategy)` to provide [a `StreamDownloadStrategy`](../../common/README.markdown)
to use for downloading the content (`OkHttp3DownloadStrategy` is highly
recommeded, with an `OkHttpClient.Builder` configured for caching)

- Call `forPackages(String...)` to check a specific list of applications
(defaults to your own application, as determined via `getPackageName()`
on your supplied `Context`)

Then, call `build()` to create the `UpdateRequest` and call
`check(UpdateRequest.Callback)` to download (if needed), parse, and examine
the catalog for possible updates.

The `UpdateRequest.Callback` that you supply needs two methods. The simple
one is `onError()`, which is called if something goes wrong while checking
the update. You are given a `Throwable` representing the issue.

The other method is `onResult()`. Here, you are given a `Map<String, PackageSpec>`
containing the available updates, or `null` if there are none. If you called
`forPackages(String...)`, there may be an element in the `Map` for each of
those pacakges; a missing element for an application ID indicates that there
is no update available. If you did not call `forPackages()`, and you got a `Map`,
then there is an update available for your app. The `PackageSpec` contains
details from the F-Droid catalog about the available update.
