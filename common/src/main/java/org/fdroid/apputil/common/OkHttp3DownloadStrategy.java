/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common;

import android.support.annotation.NonNull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

/**
 * The preferred *DownloadStrategy implementation, but it requires you to
 * include a compatible edition of OkHttp3 in your build.
 */
public class OkHttp3DownloadStrategy
  implements FileDownloadStrategy, StreamDownloadStrategy {
  private final OkHttpClient ok;

  /**
   * Basic constructor, using a default OkHttpClient instance
   */
  public OkHttp3DownloadStrategy() {
    this(new OkHttpClient.Builder());
  }

  /**
   * Constructor for where you want to configure the OkHttpClient instance
   * that gets used, such as to blend in NetCipher, CWAC-NetSecurity, etc.
   *
   * @param okHttpClient an OkHttpClient.Builder that the strategy should
   *                     use
   */
  public OkHttp3DownloadStrategy(@NonNull OkHttpClient.Builder okHttpClient) {
    this.ok=okHttpClient.build();
  }

  /**
   * @{inheritDoc}
   */
  @Override
  public void download(@NonNull final String url, @NonNull final File dest,
                       @NonNull final FileDownloadStrategy.Callback cb) {
    try {
      download(url, new okhttp3.Callback() {

        @Override
        public void onFailure(Call call, IOException e) {
          cb.onError(url, e);
        }

        @Override
        public void onResponse(Call call, Response response)
          throws IOException {
          if (response.code()==200) {
            BufferedSink sink=Okio.buffer(Okio.sink(dest));

            sink.writeAll(response.body().source());
            sink.close();
            cb.onSuccess(url);
          }
          else {
            cb.onError(url, new FileNotFoundException(url+" not found"));
          }
        }
      });
    }
    catch(Throwable t) {
      cb.onError(url, t);
    }
  }

  /**
   * @{inheritDoc}
   */
  @Override
  public void download(@NonNull final String url,
                       @NonNull final StreamDownloadStrategy.Callback cb) {
    try {
      download(url, new okhttp3.Callback() {
        @Override
        public void onFailure(Call call, IOException e) {
          cb.onError(url, e);
        }

        @Override
        public void onResponse(Call call, Response response)
          throws IOException {
          if (response.code()==200) {
            try {
              cb.onSuccess(url, response.body().byteStream());
            }
            catch (Throwable t) {
              cb.onError(url, t);
            }
          }
          else {
            cb.onError(url, new FileNotFoundException(url+" not found"));
          }
        }
      });
    }
    catch(Throwable t) {
      cb.onError(url, t);
    }
  }

  private void download(@NonNull final String url,
                        @NonNull final okhttp3.Callback cb) {
    Request req=new Request.Builder().url(url).build();

    ok.newCall(req).enqueue(cb);
  }
}
