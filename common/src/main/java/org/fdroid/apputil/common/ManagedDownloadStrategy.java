/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * FileDownloadStrategy that uses DownloadManager to do the downloading.
 * This limits the destination file to be on external storage (specifically,
 * this class limits it to getExternalCacheDir()). This strategy has
 * the advantage of being able to recover from some problems (e.g., failover
 * from WiFi to mobile data), but DownloadManager is seriously weird.
 * So long as F-Droid stays at a reasonable size, prefer OkHttp3DownloadStrategy
 * to this one.
 */
public class ManagedDownloadStrategy implements FileDownloadStrategy {
  /**
   * Implement this to build a DownloadManager.Request meeting your specifications
   */
  interface RequestBuilder {
    /**
     * Builds a DownloadManager.Request to download from the source location
     * to the destination location
     *
     * @param source the source location
     * @param dest the destination location
     * @return the DownloadManager.Request object
     */
    DownloadManager.Request buildRequest(Uri source, Uri dest);
  }

  private final Context app;
  private final RequestBuilder reqBuilder;

  /**
   * Simple constructor, using a default RequestBuilder implementation
   *
   * @param ctxt any Context will do; the strategy will hold onto the
   *             Application singleton
   */
  public ManagedDownloadStrategy(@NonNull Context ctxt) {
    this(ctxt, new StockRequestBuilder());
  }

  /**
   * Creates a strategy instance using a custom RequestBuilder
   *
   * @param ctxt any Context will do; the strategy will hold onto the
   *             Application singleton
   * @param reqBuilder a custom RequestBuilder
   */
  public ManagedDownloadStrategy(@NonNull Context ctxt,
                                 @NonNull RequestBuilder reqBuilder) {
    if (Build.VERSION.SDK_INT<Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
      throw new IllegalStateException("ManagedDownloadStrategy requires API Level 15+");
    }

    app=ctxt.getApplicationContext();
    this.reqBuilder=reqBuilder;
  }

  /**
   * @{inheritDoc}
   */
  @Override
  public void download(@NonNull final String url, @NonNull final File dest,
                       @NonNull final Callback cb) {
    File extCache=app.getExternalCacheDir();

    if (dest.getAbsolutePath().startsWith(extCache.getAbsolutePath())) {
      DownloadManager.Request req=
        reqBuilder.buildRequest(Uri.parse(url), Uri.fromFile(dest));

      app.registerReceiver(new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent i) {
          app.unregisterReceiver(this);
          updateStatus(url, cb,
            i.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1));
        }
      }, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

      DownloadManager mgr=
        (DownloadManager)app.getSystemService(Context.DOWNLOAD_SERVICE);

      mgr.enqueue(req);
    }
    else {
      throw new IllegalArgumentException("Destination must be inside getExternalCacheDir()");
    }
  }

  private void updateStatus(String url, Callback cb, long longExtra) {
    DownloadManager mgr=
      (DownloadManager)app.getSystemService(Context.DOWNLOAD_SERVICE);
    Cursor c=
      mgr.query(new DownloadManager.Query().setFilterById(longExtra));

    if (c == null) {
      cb.onError(url, new FileNotFoundException(url+" not found"));
    }
    else {
      c.moveToFirst();

      int status=c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

      c.close();

      if (status==DownloadManager.STATUS_SUCCESSFUL) {
        cb.onSuccess(url);
      }
      else {
        cb.onError(url, new FileNotFoundException(url+" not found"));
      }
    }
  }

  private static class StockRequestBuilder implements RequestBuilder {
    @Override
    public DownloadManager.Request buildRequest(Uri uri, Uri dest) {
      DownloadManager.Request req=new DownloadManager.Request(uri);

      req.setDestinationUri(dest);
      req.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE|
        DownloadManager.Request.NETWORK_WIFI);
      req.setAllowedOverRoaming(true);

      if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN) {
        req.setAllowedOverMetered(true);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
          req.setRequiresCharging(false);
          req.setRequiresDeviceIdle(false);
        }
      }

      return(req);
    }
  }
}
