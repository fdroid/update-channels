/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import java.util.ArrayList;
import java.util.List;

public class Repo {
    private String icon;
    private int maxage;
    private String name;
    private String pubkey;
    private long timestamp;
    private String url;
    private int version;
    private String description;
    private List<String> mirrors = new ArrayList<>();

    public String icon() {
        return (icon);
    }

    void icon(String icon) {
        this.icon = icon;
    }

    public int maxage() {
        return (maxage);
    }

    void maxage(int maxage) {
        this.maxage = maxage;
    }

    public String name() {
        return (name);
    }

    void name(String name) {
        this.name = name;
    }

    public String pubkey() {
        return (pubkey);
    }

    void pubkey(String pubkey) {
        this.pubkey = pubkey;
    }

    public long timestamp() {
        return (timestamp);
    }

    void timestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String url() {
        return (url);
    }

    void url(String url) {
        this.url = url;
    }

    public int version() {
        return (version);
    }

    void version(int version) {
        this.version = version;
    }

    public String description() {
        return (description);
    }

    void description(String description) {
        this.description = description;
    }

    public List<String> mirrors() {
        return (mirrors);
    }

    void addMirror(String mirror) {
        mirrors.add(mirror);
    }
}
